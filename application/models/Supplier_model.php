<?
    class Supplier_model extends CI_Model{
        public function __construct(){
			$this->load->database();
		}
        public function list_income($id='',$order="",$limit='', $start='',$like=array())
		{
			$this->db->select("*");
            $this->db->where("product_income.supplier_id=supplier.supplier_id",NULL,FALSE);
            $this->db->where("product_income.product_id=product.product_id",NULL,FALSE);
			$this->db->where("product_income.create_id=".$id,NULL,FALSE);

			if($like!=NULL)
			$this->db->like($like); 
			$this->db->limit($limit, $start);
			
			if($order!="")
			$this->db->order_by($order, "desc");
            $this->db->from("product");
            $this->db->from("supplier");
			$this->db->from("product_income");
			return $this->db->get();
		}

    }
?>