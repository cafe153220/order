<?
    class Common_model extends CI_Model{
        public function __construct(){
			$this->load->database();
		}
        public function condition_query($db='',$where=array(),$order="")
		{
			$this->db->select("*");
			$this->db->where($where);
			if($order!="")
			$this->db->order_by($order, "desc");
			$this->db->from($db);
			return $this->db->get();
		}
		public function listing($db='',$where=array(),$order="",$limit='', $start='',$like=array())
		{
			$this->db->select("*");
			if($where!=NULL)
			$this->db->where($where);
			if($like!=NULL)
			$this->db->like($like); 
			$this->db->limit($limit, $start);
			if($order!="")
			$this->db->order_by($order, "desc");
			$this->db->from($db);
			return $this->db->get();
		}
        public function no_con_query($db='',$order="")
		{
			$this->db->select('*');
			if($order!="")
			$this->db->order_by($order, "desc"); 
			$query=$this->db->get($db);
			
			return $query;
		}
		public function insert_return_id($db='',$insert=array())
		{
			$this->db->insert($db,$insert);
			return $this->db->insert_id();

		}
		public function update($db='',$insert=array(),$where=array())
		{
			$this->db->where($where);
			$this->db->update($db,$insert);
		}
		public function delete($db='',$where=array())
		{
			$this->db->where($where);
			$this->db->delete($db);
		}
    }
?>