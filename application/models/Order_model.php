<?php 

class Order_model extends CI_Model{



		public function __construct(){
			$this->load->database();
			

		}
		public function userlist()
		{
			$this->db->select();
			$this->db->where("user.user_auth=user_auth.user_auth_id",NULL,false);
			$this->db->from("user");
			$this->db->from("user_auth");
			return $this->db->get();
		}
		
		public function merge_post($id='',$order,$creater='')
		{
			$this->db->select("*");
			if($id!='')
			$this->db->where("post_main.post_id=post_main_en.post_id AND post_main.post_id=".$id,NULL,FALSE);
			else
			$this->db->where("post_main.post_id=post_main_en.post_id",NULL,FALSE);
			if($creater!='')
			$this->db->where("post_main.post_id=post_main_en.post_id AND post_main.creater_id=".$creater,NULL,FALSE);
			$this->db->from("post_main");
			$this->db->from("post_main_en");
			if($order!="")
			$this->db->order_by($order, "desc"); 
			return $this->db->get();
		}
		
		public function get_product()
		{
			$this->db->select("product_id");
			$this->db->select("CONCAT(product.product_name,'-', product.product_spec) AS Name  ");
			$this->db->where("create_id",$this->session->userdata('user_id'));
			$this->db->from("product");
			return $this->db->get();
		}

		public function statistics_list($user_id,$time=array())
		{
			// $this->db->select("SUM(order_wealth) as total , *");
			// $this->db->where("order.order_product_id=product.product_id",NULL,FALSE);
			// $this->db->group_by("product.product_id",NULL,FALSE);
			// $this->db->from("order,product");SELECT SUM(order_wealth) as total, product.product_name FROM `order`, `product` WHERE order.order_product_id=product.product_id
			$query = $this->db->query("SELECT SUM(order.order_wealth) AS Total , product.product_name FROM `order`, `product` WHERE order.order_product_id=product.product_id AND "."order.create_id = "."'".$user_id."'"." AND order.create_date BETWEEN "."'".$time["date_start"]."'"." AND '".$time["date_end"]."'"."  GROUP BY `product_id`");
			return $query->result_array();
		}

		public function list_order($id='',$order="",$limit='', $start='',$like=array())
		{
			$this->db->select("*");
			$this->db->where("order.order_product_id=product.product_id",NULL,FALSE);
			$this->db->where("order.create_id=".$id,NULL,FALSE);

			if($like!=NULL)
			$this->db->like($like); 
			$this->db->limit($limit, $start);
			
			if($order!="")
			$this->db->order_by($order, "desc");
			$this->db->from("product");
			$this->db->from("order");
			return $this->db->get();
		}

		public function user_search($where=array())
		{
			$this->db->select();
			$this->db->where("user.user_auth=user_auth.user_auth_id",NULL,false);
			if($where["user_accnum"]!="")
			$this->db->like("user.user_accnum",$where["user_accnum"],NULL,false);
			if($where["user_email"]!="")
			$this->db->like("user.user_email",$where["user_email"],NULL,false);
			if($where["user_name"]!="")
			$this->db->like("user.user_name",$where["user_name"],NULL,false);
			$this->db->from("user");
			$this->db->from("user_auth");
			return $this->db->get();
		}
       

}