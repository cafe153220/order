{
	"column_structure": 
	[	
		{
			"head": "帳 號",
			"data": "user_account",
			"datatype": "value"
		},

		{
			"head": "姓 名",
			"data": "user_name",
			"datatype": "value"
		},
		{
			"head": "信 箱",
			"data": "user_email",
			"datatype": "value"
		},
		{
			"head": "連 絡 電 話",
			"data": "user_phone",
			"datatype": "value"
		},
		{
			"head": "編輯",
			"data": "user_id",
			"datatype" : "button",
			"function" : [
							{
								"fun" : "edit",
								"text" : "編輯",
								"style" : "btn btn-outline-info btn-small",
								"parms":
								[
									{
										"value": "user_id"
									}
								]
							}
						 ]
		}
	
	]
}