<div class="container">
<div class="card">
          <div class="card-header">產品管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal"  enctype="multipart/form-data" action="<?=base_url()?>admin/user_update/<?=$user_id?>" method="post" >
									<fieldset>
									<div class="form-row">
										<div class="form-group col-md-6">
										<label for="user_account">使用者帳戶</label>
										<input type="text" class="form-control" id="user_account" required="required" name="user_account" value="user_account"  >
										</div>
										<div class="form-group col-md-6">
										<label for="user_name">使用者名稱</label>
										<input type="text" class="form-control" id="user_name" name="user_name" >
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="user_password">使用者密碼</label>
											<input type="text" class="form-control" id="user_password" name="user_password" >
										</div>
										<div class="form-group col-md-6">
										<label for="user_email">使用者E-mail</label>
										<input type="email" class="form-control" id="user_email" name="user_email" >
										</div>
									</div>
										
									
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="user_phone">連絡電話</label>
											<input type="text" class="form-control" id="user_phone" name="user_phone" >
										</div>
										<div class="form-group col-md-6">
										<label for="overdue_date">帳戶到期日</label>
										<input type="date" class="form-control" id="overdue_date" name="overdue_date" >
										</div>
									</div>
									
									
							</div><!--widge-content-->
										
									
								</fieldset>
								
          </div> 
          <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Save</button> 
		  <a href="<?=base_url()."product"?>" class="btn">Cancel</a>
		  </div>
		  </form>
</div>
</div>
                
		
    
