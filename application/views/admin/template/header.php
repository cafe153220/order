

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>訂單系統</title>

<!-- Bootstrap core CSS -->

<link href="<?=base_url()."resource/bootstrap"?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()."resource/bootstrap"?>/css/footer.css" rel="stylesheet">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>


<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/popper.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/bootstrap.min.js"></script>
<script src="<?=base_url()."resource"?>/datalist.js"></script>
</head>
  <body style="background-color:#f8f9fa;" >
  <div class="container">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">

      <a href="#" class="navbar-brand">進銷存系統</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar10">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar10">
          <ul class="navbar-nav nav-fill ">
             <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop2" data-toggle="dropdown">
                銷售管理
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url("order");?>">訂單頁面</a>
                <a class="dropdown-item" href="<?=base_url("order/statistics");?>">統計頁面</a>
              </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop3" data-toggle="dropdown">
              供應管理
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()."supplier"?>">廠商管理</a>
                <a class="dropdown-item" href="<?=base_url("supplier/supplier_income");?>">進貨頁面</a>
              </div>
          </li>
              <!-- <li class="nav-item">
                  <a class="nav-link" href="<?=base_url()."supplier"?>">供應商管理</a>
              </li> -->
              <li class="nav-item">
                  <a class="nav-link" href="<?=base_url()."product"?>">產品管理</a>
              </li>
              <?if($this->session->userdata("user_auth")=="1"){?>
              <li class="nav-item">
                  <a class="nav-link" href="<?=base_url()."admin/user_list"?>">使用者管理</a>
              </li>
              <?}?>
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <?=$this->session->userdata("user_acc")?>
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url("admin/logout");?>">登出</a>
              </div>
            </li>
          </ul>
      </div>
  </div>
  </nav>
  </div>