
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>訂單系統</title>

<!-- Bootstrap core CSS -->

<link href="<?=base_url()."resource/bootstrap"?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()."resource/login"?>/floating-labels.css" rel="stylesheet">
<script src="<?=base_url()."resource/bootstrap"?>/js/bootstrap.bundle.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/jquery-3.3.1.min.js"></script>
</head>
  <body width="500px">
  <form class="form-signin" method="post" action="<?=base_url()."admin/login_valid"?>">
      <div class="text-center mb-4">
        
        <h1 class="h3 mb-3 font-weight-normal">歡迎來到訂單管理系統</h1>
        <p>請提供您的帳號密碼</p>
      </div>

      <div class="form-label-group">
        <input type="text" name="user_accnum" id="inputEmail" class="form-control" placeholder="account" required autofocus>
        <label for="inputEmail">account</label>
      </div>

      <div class="form-label-group">
        <input type="password" name="user_password" id="inputPassword" class="form-control" placeholder="Password" required>
        <label for="inputPassword">Password</label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted text-center">&copy; Cafe</p>
    </form>

  </body>
</html>
