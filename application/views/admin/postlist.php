
  <div class="container">
      
      <br>
      <div class="card">
          <div class="card-header">訂單管理<a style="margin-left:20px" class="btn btn-outline-primary btn-small" href="<?=base_url()."admin/order_add"?>">新增訂單</a></div>
          <div class="card-body">
          <div class="table-responsive">
          <table class="table  table-striped table-hover table-info table-bordered" >
          <thead>
          <tr>
            <th scope="col"> 商 品 名 稱 </th>
            <th scope="col"> 商 品 數 量</th>
            <th scope="col"> 客 戶 名 稱</th>
            <th scope="col"> 客 戶 電 話</th>
            <th scope="col"> 交 貨 日 期</th>
            <th scope="col"> 訂 單 狀 態</th>
            <th scope="col"> 訂 單 收 益</th>
            <th class="td-actions d-inline-block col">動 作</th>
          </tr>
        </thead>
        <tbody>
        <?foreach($list as $item):?>
          <tr >
            <td> <?=$item["order_name"]?> </td>
            <td> <?=$item["order_num"]?> </td>
            <td> <?=$item["order_cus_name"]?> </td>
            <td> <?=$item["order_cus_phone"]?> </td>
            <td> <?=$item["order_date"]?> </td>
            <td> <?=$item["order_status"]?> </td>
            <td> <?=$item["order_wealth"]?> </td>
            
            <td >
            <a class="btn btn-outline-success btn-small" href="<?=base_url()."admin/edit/?id=".$item["order_id"]?>">編輯</i></a>
            <a class="btn btn-outline-danger btn-small" href="javascript:myFunction(<?=$item["order_id"]?>);">刪除</i></a>
            </td>
          </tr>
        <?endforeach?>
          </tbody>
        </table>
        
          </div>
          </div> 
          <div class="card-footer"> <?=$page?></div>
    </div>
      
  </div>
  
  <!-- <footer class="footer">
      <div class="container">
        <span class="text-muted">聯絡資訊:0912345678</span>
      </div>
    </footer> -->
  </body>
</html>
            
    <script> 

function myFunction($post_id) {

   

    if (confirm("確定要刪除?")) 

     location.href='<?=base_url()?>admin/delete_order/?id='+$post_id  ;

      

}

</script>
    
    <script>
    function click()
    {
      alert("click");

    }
    </script>
    
    