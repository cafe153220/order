<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="form"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" >使用者管理</h4>

        </button>
      </div>
      <div class="modal-body">
      <?=$add_form?>
      </div>
    </div>
  </div>
</div>
  <div class="container">
      
  <br>
  <div class="card">
      <div class="card-header">
        <div class="d-flex bd-highlight mb-12">
            <div class="table-responsive">    
                <div class="mr-auto p-2 bd-highlight ">
                  使用者管理
                </div>
                <div class="p-2 bd-highlight ">
                  <button class="btn btn-outline-primary btn-small " type="button" onClick="show_form()">新增使用者</a>
                  <!-- <a class="btn btn-outline-success btn-small " href="<?=base_url()."order/reload"?>">重新整理</a> -->
                </div>
                <!-- <form action="<?=base_url()."order/search"?>" method="post">
                <div class="p-2 bd-highlight ">
                    <div class="input-group">
                    
                      <div class="controls">
                              <select name="search_name" class="form-control">
                                  <option value="product_name">商品名稱</option>
                                  <option value="order_cus_name">客戶名稱</option>
                                  <option value="order_date">交貨日期</option>
                                </select>

                              </div>
                        <input type="text" class="form-control" name="search_val" placeholder="搜尋內容" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">搜尋</button>
                          
                        </div>
                      </div>
                    </form>
              </div> -->
          </div>
        </div>
      </div>

     <div class="list"></div>
</div>
  
</div>

<!-- <footer class="footer">
  <div class="container">
    <span class="text-muted">聯絡資訊:0912345678</span>
  </div>
</footer> -->
</body>
</html>
        
<script> 
    $('#form').on('hidden.bs.modal', function (e) {
      hide();
    })
    
    
    var obj = <?=$data_structure?>;
    $( document ).ready(function() {
        $.each(obj['column_structure'], function(key, dat){
            console.log(key,dat);
          });
        datalist("<?=base_url()."admin/user_listing"?>",dat=[],obj['column_structure']);
        
    });
    function bind(target_form,data)
    {
          console.log(data);
          var dat = target_form.serializeArray().reduce(function(obj, item) {
          obj[item.name] = item.value;
          return obj;}, {});
          console.log(dat);
          $.each(dat, function(key, dat){
          $("#"+key).val(data[key]);
          show_form();
      });
    }
    function edit(id)
    {
        $.ajax
        ({
            url:'<?=base_url()."admin/user_edit"?>',
            type : "GET",
            datatype : "text",
            data :
            {
               user_id : id
            },
            success : function(data){
               
               console.log(data);
               bind($("#edit-profile"),data);
            },
            error : function(){
              alert("Error");
            }
        });
    }
    function post_dat()
    {
      var dat =$("#edit-profile").serializeArray().reduce(function(obj, item) {
              obj[item.name] = item.value;
              return obj;}, {});
      $.ajax
      ({
          url: '<?=base_url()."admin/user_update"?>',
          type: "POST",
          datatype :"text",
          data :{
            dat: dat
          },
          success : function(data){
            datalist("<?=base_url()."admin/user_listing"?>",dat=[],obj['column_structure']);
         
          },
          error : function(){
            alert("Error");
          }
      })
    }
    function show_form()
    {
      $("#form").modal('show');
    }
    function hide()
    {
      $("#form").modal('hide');
      clear('#edit-profile ');
    }
    function clear(form)
    {
      $(form)[0].reset();
      $(form+'input[type=hidden]').val('');
    }
</script>

