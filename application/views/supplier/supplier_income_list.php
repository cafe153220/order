

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="form"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" >進貨編輯</h4>

        </button>
      </div>
      <div class="modal-body">
      <?=$add_form?>
      </div>
    </div>
  </div>
</div>
  <div class="container">
      
      <br>
      <div class="card">
          <div class="card-header">進貨管理<button type="button" style="margin-left:20px" class="btn btn-outline-primary btn-small" onClick="show()">進貨編輯</a></div>
          <div class="list"></div>
          <div class="card-footer" id="data_footer"></div>
    </div>
      
  </div>
  
  
  </body>
</html>
            
    <script> 
    $('#form').on('hidden.bs.modal', function (e) {
      hide();
    })
    
    function bind(target_form,data)
    {
          console.log(data);
          var dat = target_form.serializeArray().reduce(function(obj, item) {
          obj[item.name] = item.value;
          return obj;}, {});
          console.log(dat);
          $.each(dat, function(key, dat){
          $("#"+key).val(data[key]);
          lock(data);
          show();
      });
    }
    
    function show()
    {
      $("#form").modal('show');
    }
    function delete_income(id)
    {
      if(confirm("確定刪除?"))
      {
        $.ajax
          ({
              url:'<?=base_url()."supplier/delete_income"?>',
              type : "GET",
              datatype : "text",
              data :
              {
                id : id
              },
              success : function(data){
                datalist("<?=base_url()."supplier/income_listing"?>",dat=[],obj['column_structure']);
              },
              error : function(){
                alert("Error");
              }
          }); 
      }
    }
    function check_permittion()
    {
        var dat =$("#edit-profile").serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;}, {});
        $.ajax
        ({
            url: '<?=base_url()."supplier/income_check_permittion"?>',
            type: "POST",
            datatype :"text",
            data :{
              dat: dat
            },
            success : function(data){
              var check_result=data;
              
              if(check_result.success=="warning")
              {
                  if(confirm(check_result.msg))
                  {
                    if(check_result.lock=="income_lock")
                    {
                      
                      dat["income_lock"]="1";
                      dat["income_num_lock"]="1";
                    }
                    else if(check_result.lock=="income_num_lock")
                    {
                      
                      dat["income_num_lock"]="1";
                    }
                  }
                  post_dat(dat);
              }
              else
              {
                  post_dat(dat);
              }
             
            },
            error : function(){
              alert("Error");
            }
        });
    }
   function hide()
    {
      $('#form').modal('hide');
      clear('#edit-profile ');
    }
function clear(form)
    {
      $(form)[0].reset();
      $(form+' input[type=hidden]').val('');
      $(form+" select").removeAttr('disabled');
      $(form+" select").removeAttr('readonly');
      $(form+" input").removeAttr('readonly');
      $(form+" textarea").attr("readonly", false);
    }
    function edit(id)
    {
        $.ajax
        ({
            url:'<?=base_url()."supplier/income_edit"?>',
            type : "GET",
            datatype : "text",
            data :
            {
               income_id : id
            },
            success : function(data){
               
               console.log(data);
               bind($("#edit-profile"),data);
            },
            error : function(){
              alert("Error");
            }
        });
    }
    function lock(data)
    {
      console.log("lock",data);
      if(data["income_lock"]=='1')
      {
        $.each(data, function(key, dat){
          $("#"+key).attr('readonly', true);
          $("#edit-profile select").attr("disabled", true); 
        });
        
      }
      if(data["income_num_lock"]=='1')
        {
          $("#product_num").attr('readonly', true);
              
        }
    } 
    function post_dat(dat)
    {
     
      $.ajax
      ({
          url: '<?=base_url()."supplier/supplier_income_update"?>',
          type: "POST",
          datatype :"text",
          data :{
            dat: dat
          },
          success : function(data){
          datalist("<?=base_url()."supplier/income_listing"?>",dat=[],obj['column_structure']);
          console.log(data);
          },
          error : function(){
            alert("Error");
          }
      })
    }
var obj = <?=$data_structure?>;
$( document ).ready(function() {
     
     
     $.each(obj['column_structure'], function(key, dat){
         console.log(key,dat);
       });
     datalist("<?=base_url()."supplier/income_listing"?>",dat=[],obj['column_structure']);
     
});


</script>
    
    <script>
    function click()
    {
      alert("click");

    }
    </script>
    
    