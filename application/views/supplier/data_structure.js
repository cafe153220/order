{
	"column_structure": 
	[	
		{
			"head": "供 應 商 名 稱",
			"data": "supplier_name",
			"datatype": "value"
		},

		{
			"head": "供 應 商 電 話",
			"data": "supplier_phone",
			"datatype": "value"
		},
		{
			"head": "供 應 商 地 點",
			"data": "supplier_location",
			"datatype": "value"
		},
		{
			"head": "動作",
			"data": "supplier_id",
			"datatype" : "button",
			"function" : [
							{
								"fun" : "edit",
								"text" : "編輯",
								"style" : "btn btn-outline-info btn-small",
								"parms":
								[
									{
										"value": "supplier_id"
									}
								]
							},
                            {
								"fun" : "delete_supplier",
								"text" : "刪除",
								"style" : "btn btn-outline-danger btn-small",
								"parms":
								[
									{
										"value": "supplier_id"
									}
								]
							}
						 ]
		}
	
	]
}