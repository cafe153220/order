{
	"column_structure": 
	[	
		{
			"head": "供 應 商 名 稱",
			"data": "supplier_name",
			"datatype": "value"
		},

		{
			"head": "產 品",
			"data": "product_name",
			"datatype": "value"
		},
		{
			"head": "進 貨 數 量",
			"data": "product_num",
			"datatype": "value"
		},
        {
			"head": "定 貨 狀 態",
			"data": "income_status",
			"datatype": "value"
		},
        {
			"head": "定 貨 金 額",
			"data": "product_cost",
			"datatype": "value"
		},
        {
			"head": "運 貨 日 期",
			"data": "ship_date",
			"datatype": "value"
		},
		{
			"head": "動作",
			"data": "income_id",
			"datatype" : "button",
			"function" : [
							{
								"fun" : "edit",
								"text" : "編輯",
								"style" : "btn btn-outline-info btn-small",
								"parms":
								[
									{
										"value": "income_id"
									}
								]
							},
                            {
								"fun" : "delete_income",
								"text" : "刪除",
								"style" : "btn btn-outline-danger btn-small",
								"parms":
								[
									{
										"value": "income_id"
									}
								]
							}
						 ]
		}
	
	]
}