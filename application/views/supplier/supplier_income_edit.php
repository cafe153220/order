<div class="container">
<div class="card">
          <div class="card-header">進貨管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal" >
									<fieldset>
									
										<div class="form-row">
                                            <div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >供應商</label>
													<div class="controls">
													<select id="supplier_id"  name="supplier_id" class="form-control">
															<?foreach($supplier as $item):?>
															<option value="<?=$item["supplier_id"]?>"><?=$item["supplier_name"]?></option>
															<?endforeach?>
															
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >產品</label>
													<div class="controls">
													<select id="product_id"  name="product_id" class="form-control">
															<?foreach($products as $item):?>
															<option value="<?=$item["product_id"]?>"><?=$item["product_name"]?></option>
															<?endforeach?>
															
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="product_num">訂貨數量</label>
											<input type="text" class="form-control" required="required" id="product_num" name="product_num">
											</div>
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >訂貨狀態</label>
													<div class="controls">
													<select id="income_status"  name="income_status" class="form-control">
															<option value="未進貨 - 未付款">未進貨 - 未付款</option>
															<option value="進貨 - 未付款">進貨 - 未付款</option>
															<option value="未進貨 - 付款完成">未進貨 - 付款完成</option>
															<option value="進貨 - 付款完成">進貨 - 付款完成</option>
													</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_income">訂貨金額</label>
											<input type="text" class="form-control" id="product_cost" name="product_cost" value="0" >
											</div>
											<div class="form-group col-md-6">
											<label for="order_income">運貨日期</label>
											<input type="date" class="form-control" id="ship_date" name="ship_date"  >
											</div>
										</div><!--/form-row-->
										
                                        <div class="control-group">											
											<label class="control-label" for="lastname">訂貨備註</label>
											<div class="controls">
												<textarea name="supplier_income_remark" id="supplier_income_remark"  rows="10" cols="80" class="form-control"></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                        <input type="hidden" class="form-control" id="income_id"  name="income_id">
										<input type="hidden" class="form-control" id="income_lock"  name="income_lock">
										<input type="hidden" class="form-control" id="income_num_lock"  name="income_num_lock">
										
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  
		  <button type="button" onClick="check_permittion()" id="add_order" class="btn btn-primary">Save</button> 
		  <button type="button" onClick="hide()" class="btn">Cancel</button>
		  </div>
		  </form>
</div>
</div>
                
		
    
<script>
// $('#income').keyup(function() { 
// 		var cost= $('#cost').val();
// 		var income =$('#income').val();
// 		console.log(cost);
// 		console.log(income);
// 		$('#wealth').val(income-cost);
//     });

</script>