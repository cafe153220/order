<div class="container">
<div class="card">
          <div class="card-header">供應商管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal"  enctype="multipart/form-data" action="<?=base_url()?>supplier/supplier_update" method="post" >
									<fieldset>
									<div class="form-row">
											<div class="form-group col-md-6">
											<label for="supplier_name">供貨商名稱</label>
											<input type="text" class="form-control" id="supplier_name" required="required" name="supplier_name">
											</div>
											<div class="form-group col-md-6">
											<label for="supplier_phone">供應商電話</label>
											<input type="text" class="form-control" id="supplier_phone" name="supplier_phone">
											</div>
										</div>
										
										

										<div class="form-row">
											<label for="supplier_location">供應商地址</label>
											<input type="text" class="form-control" required="required" id="supplier_location" name="supplier_location">
											
										</div>
                                        <div class="control-group">											
											<label class="control-label" for="supplier_remark">供應商備註</label>
											<div class="controls">
												<textarea name="supplier_remark" id="supplier_remark"  rows="10" cols="80" class="form-control"></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<input type="hidden" class="form-control" required="required" id="supplier_id" name="supplier_id">
                                        
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  <button type="button" onClick="post_dat()" class="btn btn-primary">Save</button> 
		  <button type="button" onClick="hide()" class="btn">Cancel</button>
		  </div>
		  </form>
</div>
</div>
                
		
    
