<div class="container">
<div class="card">
          <div class="card-header">產品管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal"  enctype="multipart/form-data" action="<?=base_url()?>product/product_update/?id=<?=$supplier_id?>" method="post" >
									<fieldset>
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="product_name">產品名稱</label>
											<input type="text" class="form-control" id="product_namee" required="required" name="product_name" value="<?=$product_name?>" >
											</div>
											<div class="form-group col-md-6">
											<label for="product_spec">產品規格</label>
											<input type="text" class="form-control" id="product_spec" name="product_spec" value="<?=$product_spec?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="supplier">供應商</label>
													<select id="supplier" name="supplier_id" class="form-control">
													<option value="1">測試供應商</option>
													
												</select>
											</div>
											<div class="form-group col-md-6">
											<label for="product_stock">產品數量</label>
											<input type="text" class="form-control" id="product_stock" name="product_stock" value="<?=$product_stock?>">
											</div>
										</div>
											
										
                                        <div class="control-group">											
											<label class="control-label" for="product_remark">產品備註</label>
											<div class="controls">
												<textarea name="product_remark" id="product_remark"  rows="10" cols="80" class="form-control"><?=$product_remark?></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                        
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Save</button> 
		  <a href="<?=base_url()."product"?>" class="btn">Cancel</a>
		  </div>
		  </form>
</div>
</div>
                
		
    
