{
	"column_structure": 
	[	
		{
			"head": "商 品 名 稱",
			"data": "product_name",
			"datatype": "value"
		},

		{
			"head": "產 品 規 格",
			"data": "product_spec",
			"datatype": "value"
		},
		{
			"head": "產 品 存 量",
			"data": "product_stock",
			"datatype": "value"
		},
		{
			"head": "產 品 備 註",
			"data": "product_id",
            "datatype": "button",
            "function": [
                            {
                                "fun" : "show",
                                "text" : "更多細節",
                                "style" : "btn btn-outline-success btn-small",
                                "parms": 
                                [{
                                    "value": "product_id"
                                }]
                            }
                        ]
		},
		{
			"head": "編輯",
			"data": "product_id",
			"datatype" : "button",
			"function" : [
							{
								"fun" : "edit",
								"text" : "編輯",
								"style" : "btn btn-outline-info btn-small",
								"parms":
								[
									{
										"value": "product_id"
									}
								]
							}
						 ]
		}
	
	]
}