

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="mymodal"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="form"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" >產品編輯</h4>

        </button>
      </div>
      <div class="modal-body">
      <?=$add_form?>
      </div>
    </div>
  </div>
</div>
  <div class="container">
      
      <br>
      <div class="card">
          <div class="card-header">
          <div class="table-responsive">    
                <div class="mr-auto p-2 bd-highlight ">
                  產品管理
                </div>
                <div class="p-2 bd-highlight ">
                  <button type="button" class="btn btn-outline-primary btn-small " onClick="show_form()">新增產品</button>
                  <button type="button" class="btn btn-outline-success btn-small " onClick="reload_form()">重新整理</a>
                </div>
                <form id="search_form" method="post">
                <div class="p-2 bd-highlight ">
                    <div class="input-group">
                    
                      <div class="controls">
                              <select name="search_name" class="form-control">
                                  <option value="product_name">商品名稱</option>
                                  <!-- <option value="order_cus_name">客戶名稱</option>
                                  <option value="order_date">交貨日期</option> -->
                                </select>

                              </div>
                        <input type="text" class="form-control" name="search_val" placeholder="搜尋內容" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" onClick="search_product($('#search_form'))">搜尋</button>
                          
                        </div>
                      </div>
                    </form>
              </div>
          </div>
          </div>
          <div class="list"></div>
          <div class="card-footer" id="data_footer"></div>
    </div>
      
  </div>
  
  <!-- <footer class="footer">
      <div class="container">
        <span class="text-muted">聯絡資訊:0912345678</span>
      </div>
    </footer> -->
  </body>
</html>
            
    <script> 
     $('#form').on('hidden.bs.modal', function (e) {
     
      hide();
    })
    $('#mymodal').on('show.bs.modal', function (e) {
      $('#mymodal').find("h4.modal-title").html("<i class='fas fa-spinner fa-spin '></i> 讀取中");
  $('#mymodal').find("div.modal-body").html("");
 
})
var obj = <?=$data_structure?>;
$( document ).ready(function() {
     
     
     $.each(obj['column_structure'], function(key, dat){
         console.log(key,dat);
       });
     datalist("<?=base_url()."product/listing"?>",dat=[],obj['column_structure']);
     
});
function reload_form()
{
  $.ajax({
        url : '<?=base_url()."product/reload"?>',
        type : "POST",
        datatype : "text",
        data: {
        
        },
        success : function(data){
        datalist("<?=base_url()."product/listing"?>",dat=[],obj['column_structure']);
        console.log(data);
     },
     error : function(){
       alert("Error");
     }

      });
}
function search_product(form_name)
{
  var dat =form_name.serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;}, {});
        console.log("dat",dat);
      $.ajax({
        url : '<?=base_url()."product/search"?>',
        type : "POST",
        datatype : "text",
        data: {
          dat : dat
        },
        success : function(data){
        datalist("<?=base_url()."product/listing"?>",dat=[],obj['column_structure']);
        console.log(data);
     },
     error : function(){
       alert("Error");
     }

      });
}

function post_dat()
{
  var dat = $('#edit-profile').serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;}, {});
     $.ajax({
       url: <?="'".base_url()."product/product_update'"?>,
       type: "POST",
       dataType: "text",
       data : {
          dat: dat
       },
       success : function(data){
        datalist("<?=base_url()."product/listing"?>",dat=[],obj['column_structure']);
        console.log(data);
     },
     error : function(){
       alert("Error");
     }
     })
}
function edit(product_id)
{
    $.ajax({
      url: <?="'".base_url()."product/get_product'"?>,
      type : "GET",
      dataType : "text",
      data : {
        product_id :　product_id
      },
      success : function(data){
        result=JSON.parse(data);//$('#edit-profile')
        bind($('#edit-profile'),result);
        
     },
     error : function(){
       alert("Error");
     }

    })
}
function bind(target_form,data)
{
    console.log(data);
    var dat = target_form.serializeArray().reduce(function(obj, item) {
    obj[item.name] = item.value;
    return obj;}, {});
    console.log(dat);
    $.each(dat, function(key, dat){
    $("#"+key).val(data[key]);
    show_form();
});
}
function show_form()
{
  $('#form').modal('show');
}
function show(product_id='')
{
 
  $('#mymodal').modal('show')
   $.ajax({
    url: "<?=base_url()."product/get_remark"?>",
    type : "POST",
    dataType : "text",
    data : {
      product_id:product_id
    },
    success : function(data){
      
      var result =JSON.parse(data);
      console.log(result); 
      $("#mymodal h4.modal-title").html(result.head);
      $("#mymodal div.modal-body").html(result.remark);
      ;
    },
    error : function(){
      alert("Error");
    }

   });
  
}
function myFunction($post_id) {

   

    if (confirm("確定要刪除?")) 

     location.href='<?=base_url()?>product/delete_product/?id='+$post_id  ;

      

}

function hide()
{
  $('#form').modal('hide');
  clear('#edit-profile ');
}
function clear(form)
    {
      $(form)[0].reset();
      $(form+'input[type=hidden]').val('');
    }
</script>
    
   