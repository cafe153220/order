<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>工研院</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?=base_url()."resource/admin/"?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()."resource/admin/"?>css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="<?=base_url()."resource/admin/"?>css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()."resource/admin/"?>css/style.css" rel="stylesheet">
    <script src="<?=base_url()."resource/";?>ckeditor/ckeditor.js"></script>
	<script src="<?=base_url()."resource/";?>ckfinder/ckfinder.js"></script>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<?foreach($files as $file):?>
<a download href="<?=base_url().$dir.$file?>"><?=$file?></a>
<a class="btn btn-danger btn-small" href="javascript:myfunction('<?=$dir?>','<?=$file?>')">
<i class="btn-icon-only icon-remove"> </i>
</a>
<?endforeach?>
<script>
function myfunction($dir,$file)
{
    location.href='<?=base_url()?>admin/delete_index_file/?dir='+$dir+'&file='+$file;

}
</script>
</html>
