{
	"column_structure": 
	[	
		{
			"head": "商 品 名 稱",
			"data": "product_name",
			"datatype": "value"
		},

		{
			"head": "商 品 數 量",
			"data": "order_num",
			"datatype": "value"
		},
		{
			"head": "客 戶 名 稱",
			"data": "order_cus_name",
			"datatype": "value"
		},
		{
			"head": "客 戶 電 話",
			"data": "order_cus_phone",
			"datatype": "value"
		},
		{
			"head": "交 貨 日 期",
			"data": "order_date",
			"datatype": "value"
		},
		{
			"head": "訂 單 狀 態",
			"data": "order_status",
			"datatype": "value"
		},
		{
			"head": "訂 單 收 益",
			"data": "order_wealth",
			"datatype": "value"
		},
		{
			"head": "動作",
			"data": "product_id",
			"datatype": "button",
			"function": [{
					"fun": "edit",
					"text" : "編輯",
					"style":"btn btn-outline-success btn-small",
					"parms": 
					[{
						"value": "order_id"
					}]
				},
				{
					"fun": "remove",
					"text" : "刪除",
					"style":"btn btn-outline-danger btn-small",
					"parms": 
					[{
						"value": "order_id"
					}]
				}
			]
		}
	
	]
}