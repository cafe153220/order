<div class="container">
<div class="card">
          <div class="card-header">訂單管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal" >
									<fieldset>
									<div class="form-row">
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >產品名稱</label>
													<div class="controls">
													<select id="order_product_id"  name="order_product_id" class="form-control">
															<?foreach($products as $item):?>
															<option value="<?=$item["product_id"]?>"><?=$item["Name"]?></option>
															<?endforeach?>
															
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
											<div class="form-group col-md-6">
											<label for="order_num">商品數量</label>
											<input type="text" class="form-control" id="order_num" title="訂單不得為負數"  name="order_num">
											</div>
										</div><!--/form-row-->
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_name">訂單客戶</label>
											<input type="text" class="form-control" id="order_cus_name" required="required"  name="order_cus_name">
											</div>
											<div class="form-group col-md-6">
											<label for="order_num">訂單客戶電話</label>
											<input type="text" class="form-control" id="order_cus_phone" name="order_cus_phone">
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_name">交貨地點</label>
											<input type="text" class="form-control" required="required" id="order_location" name="order_location">
											</div>
											<div class="form-group col-md-6">
											<label for="order_num">交貨日期</label>
											<input type="date" class="form-control" id="order_date" name="order_date">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >訂單狀態</label>
													<div class="controls">
													<select id="order_status"  name="order_status" class="form-control">
													
														<optgroup label="訂貨前">
															
															<option value="未確認訂單">未確認訂單</option>
														</optgroup>
														<optgroup label="訂貨後">
															<option value="未交貨 - 未收款項">未交貨 - 未收款項</option>
															<option value="未交貨 - 已收款項">未交貨 - 已收款項</option>
															<option value="已交貨 - 未收款項">已交貨 - 未收款項</option>
															<option value="訂單完成">訂單完成</option>
														</optgroup>
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
											<div class="form-group col-md-6">
											<label for="order_income">已收金額</label>
											<input type="text" class="form-control" id="order_income" name="order_income" value="0" onchange="cal_wealth()">
											</div>
										</div><!--/form-row-->
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_cost">進貨成本</label>
											<input type="text" class="form-control" id="order_cost" name="order_cost" value="0" onchange="cal_wealth()" >
											</div>
											<div class="form-group col-md-6">
											<label for="order_wealth">訂單收益</label>
											<input type="text" class="form-control" id="order_wealth" name="order_wealth">
											</div>
										</div>
                                        <div class="control-group">											
											<label class="control-label" for="lastname">訂單備註</label>
											<div class="controls">
												<textarea name="order_remark" id="order_remark"  rows="10" cols="80" class="form-control"></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                        <input type="hidden" class="form-control" id="order_id"  name="order_id">
										<input type="hidden" class="form-control" id="order_num_lock"  name="order_num_lock">
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  
		  <button type="button" onClick="check_permittion()" id="add_order" class="btn btn-primary">Save</button> 
		  <button type="button" onClick="hide()" class="btn">Cancel</button>
		  </div>
		  </form>
</div>
</div>
                
		
    
<script>
// $('#income').keyup(function() { 
// 		var cost= $('#cost').val();
// 		var income =$('#income').val();
// 		console.log(cost);
// 		console.log(income);
// 		$('#wealth').val(income-cost);
//     });
function cal_wealth()
{
	var cost= $('#order_cost').val();
		var income =$('#order_income').val();
		console.log(cost);
		console.log(income);
		$('#order_wealth').val(income-cost);
}
</script>