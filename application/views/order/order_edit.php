<div class="container">
<div class="card">
          <div class="card-header">訂單管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal"  enctype="multipart/form-data" action="<?=base_url()?>order/order_update/?id=<?=$order_id?>" method="post" >
									<fieldset>
									<div class="form-row">
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >訂單狀態</label>
													<div class="controls">
													<select  name="order_product_id" class="form-control" >
															<?foreach($products as $item):?>
															<option value="<?=$item["product_id"]?>" <?if($order_product_id==$item["product_id"]) echo "selected"?>><?=$item["Name"]?></option>
															<?endforeach?>
															
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
											<div class="form-group col-md-6">
											<label for="order_num">商品數量</label>
											<input type="text" class="form-control" id="order_num" name="order_num" value=<?=$order_num?>>
											</div>
										</div><!--/form-row-->
										
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_name">訂單客戶</label>
											<input type="text" class="form-control" id="order_cus_name" required="required"  name="order_cus_name" value="<?=$order_cus_name?>" >
											</div>
											<div class="form-group col-md-6">
											<label for="order_num">訂單客戶電話</label>
											<input type="text" class="form-control" id="order_cus_phone" name="order_cus_phone" value="<?=$order_cus_phone?>">
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_name">交貨地點</label>
											<input type="text" class="form-control" required="required" id="order_location" name="order_location" value="<?=$order_location?>">
											</div>
											<div class="form-group col-md-6">
											<label for="order_num">交貨日期</label>
											<input type="date" class="form-control" id="order_date" name="order_date" value="<?=$order_date?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<div class="control-group">											
													<label class="control-label" for="firstname" >訂單狀態</label>
													<div class="controls">
													<select  name="order_status" class="form-control">
													
														<optgroup label="訂貨前">
															<option value="已確認訂單" <?if($order_status=="已確認訂單") echo "selected" ?>>已確認訂單</option>
															<option value="未確認訂單" <?if($order_status=="未確認訂單") echo "selected" ?>> 未確認訂單</option>
														</optgroup>
														<optgroup label="訂貨後">
															<option value="未交貨 - 未收款項" <?if($order_status=="未交貨 - 未收款項") echo "selected" ?> >未交貨 - 未收款項</option>
															<option value="未交貨 - 已收款項" <?if($order_status=="未交貨 - 已收款項") echo "selected" ?> >未交貨 - 已收款項</option>
															<option value="已交貨 - 未收款項" <?if($order_status=="已交貨 - 未收款項") echo "selected" ?> >已交貨 - 未收款項</option>
															<option value="訂單完成" <?if($order_status=="訂單完成") echo "selected" ?> >訂單完成</option>
														</optgroup>
														</select>

													</div> <!-- /controls -->				
												</div> <!-- /control-group -->
											</div><!--form-group-->
											<div class="form-group col-md-6">
											<label for="income">已收金額</label>
											<input type="text" class="form-control" id="income" name="order_income"onchange="cal_wealth()" value="<?=$order_income?>">
											</div>
										</div><!--/form-row-->
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="cost">進貨成本</label>
											<input type="text" class="form-control" id="cost" name="order_cost" onchange="cal_wealth()" value="<?=$order_cost?>">
											</div>
											<div class="form-group col-md-6">
											<label for="wealth">訂單收益</label>
											<input type="text" class="form-control" id="wealth" name="order_wealth" readonly="readonly" value="<?=$order_wealth?>">
											</div>
										</div>
                                        <div class="control-group">											
											<label class="control-label" for="lastname">訂單備註</label>
											<div class="controls">
												<textarea name="order_remark" id="order_remark"  rows="10" cols="80" class="form-control"><?=$order_remark?></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                        
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Save</button> 
		  <a href="<?=base_url()."order"?>" class="btn">Cancel</a>
		  </div>
		  </form>
</div>
</div>
                
		
    
<script>
// $('#income').keyup(function() { 
// 		var cost= $('#cost').val();
// 		var income =$('#income').val();
// 		console.log(cost);
// 		console.log(income);
// 		$('#wealth').val(income-cost);
//     });
function cal_wealth()
{
	var cost= $('#cost').val();
		var income =$('#income').val();
		console.log(cost);
		console.log(income);
		$('#wealth').val(income-cost);
}
</script>