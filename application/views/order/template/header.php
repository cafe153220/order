

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>訂單系統</title>

<!-- Bootstrap core CSS -->

<link href="<?=base_url()."resource/bootstrap"?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()."resource/bootstrap"?>/css/footer.css" rel="stylesheet">
<script src="<?=base_url()."resource/bootstrap"?>/js/jquery-slim.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/popper.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/bootstrap.min.js"></script>
</head>
  <body style="background-color:#f8f9fa;" >
  <div class="container">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">

      <a href="#" class="navbar-brand">進銷存系統</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar10">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar10">
          <ul class="navbar-nav nav-fill ">
          
              <li class="nav-item">
                  <a class="nav-link" href="<?=base_url()."order"?>">銷售管理</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="<?=base_url()."supplier"?>">進貨管理</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">存貨管理</a>
              </li>
              
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <?=$this->session->userdata("user_acc")?>
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url("admin/logout");?>">登出</a>
              </div>
            </li>
          </ul>
      </div>
  </div>
  </nav>
  </div>