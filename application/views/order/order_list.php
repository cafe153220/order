<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="mymodal"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">訂單編輯</h4>
        
      </div>
      <div class="modal-body">
      <?=$add_view?>
      </div>
    </div>
  </div>
</div>
  <div class="container">
      
      <br>
      <div class="card">
          <div class="card-header">
            <div class="d-flex bd-highlight mb-12">
                <div class="table-responsive">    
                    <div class="mr-auto p-2 bd-highlight ">
                      訂單管理
                    </div>
                    <div class="p-2 bd-highlight ">
                      <a class="btn btn-outline-primary btn-small " href="javascript:show();">新增訂單</a>
                      <button class="btn btn-outline-success btn-small " onClick="reload()">重新整理</a>
                    </div>
                    <form id="search_form" action="<?=base_url()."order/search"?>" method="post">
                    <div class="p-2 bd-highlight ">
                        <div class="input-group">
                        
                          <div class="controls">
                                  <select name="search_name" class="form-control">
                                      <option value="product_name">商品名稱</option>
                                      <option value="order_cus_name">客戶名稱</option>
                                      <option value="order_date">交貨日期</option>
                                    </select>

                                  </div>
                            <input type="text" class="form-control" name="search_val" placeholder="搜尋內容" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                              <button class="btn btn-outline-secondary" onClick="search()" type="button">搜尋</button>
                              
                            </div>
                          </div>
                        </form>
                  </div>
              </div>
            </div>
          </div>
          <div class="list"></div>
          <div class="card-footer" id="data_footer"></div>
    </div>
      
  </div>
  
 
  </body>
  
</html>
   
    <script> 
    $('#mymodal').on('hidden.bs.modal', function (e) {
      hide();
      $("#order_num").attr('readonly', false);
    })
   
    function search()
    {
      var dat = $("#search_form").serializeArray().reduce(function(obj, item) {
          obj[item.name] = item.value;
          return obj;}, {});
      console.log(dat);
      $.ajax({
        url : <?="'".base_url()."order/search'"?>, 
        type : "POST",
        dataType : "text",
        data:{
          data:dat
        },
        success : function(data){
          console.log("search",data);
          datalist("<?=base_url()."order/read_order"?>",dat=[],obj['column_structure']);
        },
        error : function(){
          alert("Error");
        }
      });
    }
    function reload()
    {
      dat=[];
      $.ajax({
        url : <?="'".base_url()."order/reload'"?>, 
        type : "POST",
        dataType : "text",
        data:{
          
        },
        success : function(data){
          console.log("search",data);
          datalist("<?=base_url()."order/read_order"?>",dat=[],obj['column_structure']);
        },
        error : function(){
          alert("Error");
        }
      });
    }
   function bind(target_form,data)
   {
          console.log(data);
          
          $.each(data, function(key, dat){
            $("#"+key).val(data[key]);
           
            show();
            console.log(key,data[key]);
          });
          lock(data);
          
   }
   function lock(data)
   {
     console.log("lock_data",data);
    if(data["order_lock"]=='1')
    {
      $.each(data, function(key, dat){
        $("#"+key).attr('readonly', true);
        $("#edit-profile select").attr("disabled", true); 
      });
    }
     if(data["order_num_lock"]=='1')
     {
        $("#order_num").attr('readonly', true);
          
     }
   }
   function edit(order_id){
     $.ajax({
        url: <?="'".base_url()."order/get_order"."'"?>,
        type : "POST",
        dataType : "text",
        data:{
          order_id:order_id
        },
        success : function(data){
          result=JSON.parse(data);//$('#edit-profile')
          bind($('#edit-profile'),result);
        },
        error : function(){
          alert("Error");
        }
      });
  }
function check_permittion()
{
  var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
    if ($("#add_order").html() !== loadingText) {
      $("#add_order").data('original', "123");
      $("#add_order").html(loadingText);
    }
  var dat = $('#edit-profile').serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;}, {});
   $.ajax({
     url: <?="'".base_url()."order/check_permittion'"?>,
     type: "POST",
     dataType : "text",
     data:{
       dat:dat
     },
     success : function(data){
      var result=JSON.parse(data);
      if(result["success"]==true)
      post_dat();
      else if(result["success"]=="warning")
      {
          if(confirm(result["msg"]))
          post_dat(result["lock"]);
      }
      else if(result["success"]==false)
      alert(result["msg"]);
     },
     error : function(){
       alert("Error");
     }
   });
   $("#add_order").html("Save");
}
function post_dat(lock='')
{
  
  var dat = $('#edit-profile').serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;}, {});
  if(lock!='')
  {
     dat[lock]='1';
  }
   $.ajax({
     url: <?="'".base_url()."order/order_update'"?>,
     type: "POST",
     dataType : "text",
     data:{
       dat:dat
     },
     success : function(data){
      
      $("#add_order").html("Save");
      hide();
      datalist("<?=base_url()."order/read_order"?>",dat=[],obj['column_structure']);
     },
     error : function(){
       alert("Error");
     }
   });
   
}

function remove(post_id)
{
  if (confirm("確定要刪除?")){
  // location.href='<?=base_url()?>order/delete_order/?id='+$post_id  ;
  $.ajax({
    url : <?="'".base_url()."order/delete_order"."'"?>,
    type : "POST",
    datatype : "text",
    data:{
        order_id : post_id
    },
    success : function(data){
      datalist("<?=base_url()."order/read_order"?>",dat=[],obj['column_structure']);
    },
    error : function(){
      alert("Error");
    }
  });
  }
}

function myFunction() {

   

    

      

}

</script>
    
    <script>
    var obj = <?=$data_structure?>;
    $( document ).ready(function() {
     
     
      $.each(obj['column_structure'], function(key, dat){
          console.log(key,dat);
        });
      datalist("<?=base_url()."order/read_order"?>",dat=[],obj['column_structure']);
      
});

    // $(function() {
    //   paginate();
    // });

    // function paginate() {
    //   $(document).on('click', '#ajax_pg a', function(event) {
    //       var url = $(this).attr('href');
    //       datalist(url,dat=[]);
    //       return false;
    //   }).click(); //to trigger click event 
    // }
    // function pagination()
    // {
    //   $.ajax({
    //     url: "<?=base_url()."order/pagination"?>",
    //     success : function(data){
    //       $( "div.card-footer" ).html(data);
    //     },
        
    //   });
    //   return false;
    // }
    function show()
    {
      $('#mymodal').modal('show');

    }
    function hide()
    {
      $('#mymodal').modal('hide');
      clear('#edit-profile ');
      
    }
    function clear(form)
    {
      $(form)[0].reset();
      $(form+' input[type=hidden]').val('');
      $(form+" select").removeAttr('disabled');
      $(form+" select").removeAttr('readonly');
      $(form+" input").removeAttr('readonly');
      $(form+" textarea").attr("readonly", false);
    }
    </script>
    
    