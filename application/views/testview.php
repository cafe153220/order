
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>訂單系統</title>

<!-- Bootstrap core CSS -->

<link href="<?=base_url()."resource/bootstrap"?>/css/bootstrap.min.css" rel="stylesheet">
<script src="<?=base_url()."resource/bootstrap"?>/js/jquery-slim.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/popper.min.js"></script>
<script src="<?=base_url()."resource/bootstrap"?>/js/bootstrap.min.js"></script>
</head>
  <body >
  <div class="container">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">

      <a href="/" class="navbar-brand">Bootstrap 4</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar10">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar10">
          <ul class="navbar-nav nav-fill ">
              <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="//codeply.com">Codeply</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Dropdown link
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
              </div>
            </li>
          </ul>
      </div>
  </div>
  </nav>
  </div>
  <div class="container">
      
      <br>
      <div class="card">
          <div class="card-header">訂單管理</div>
          <div class="card-body">
          <div class="table-responsive">
          <table class="table table-striped table-hover table-info">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Handle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
              
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td colspan="2">Larry the Bird</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </table>
          </div>
          </div> 
          <div class="card-footer">Footer</div>
    </div>
      
  </div>
  <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar10">
          <span class="navbar-toggler-icon"></span>
      </button> -->
  <!-- Links -->
  <!-- <div class="navbar-collapse collapse" id="navbar10">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#">Link 1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link 2</a>
    </li>
    <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
      Dropdown link
    </a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">Link 1</a>
      <a class="dropdown-item" href="#">Link 2</a>
      <a class="dropdown-item" href="#">Link 3</a>
    </div>
  </li>
  </ul>
  </div>
</nav> -->
  </body>
</html>
