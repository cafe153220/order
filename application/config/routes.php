<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//------後台路由----------//
$route['admin']= 'admin/index';
$route['admin/logout']= 'admin/logout';
$route['admin/login']= 'admin/login';
$route['admin/login_valid']= 'admin/login_valid';
$route['admin/delete_news']= 'admin/delete_news';
$route['admin/news_update']= 'admin/news_update';
$route['admin/news_add']= 'admin/news_add';
$route['admin/news_edit']= 'admin/news_edit';
$route['admin/news_list']= 'admin/news_list';
$route['admin/post_list']= 'admin/post_list';
$route['admin/delete']= 'admin/delete';
$route['admin/post']= 'admin/post';
$route['admin/edit']= 'admin/edit';
$route['admin/user_list']= 'admin/user_list';
$route['admin/delete_file']= 'admin/delete_file';
$route['admin/post_add']= 'admin/post_add';
//------前台路由----------//
$route['news'] = 'index/news';
$route['sign'] = 'index/sign';
$route['signup'] = 'index/signup';
$route['sign_en'] = 'index/sign_en';
$route['data_list'] = 'index/data_list';
$route['data_list_en'] = 'index/data_list_en';
$route['news_detail'] = 'index/news_detail';
$route['contact_us'] = 'index/contact_us';
$route['contact_insert']= 'index/contact_insert';
$route['contact_us_en'] = 'index/contact_us_en';
$route['post_detail'] = 'index/post_detail';
$route['post_detail_en'] = 'index/post_detail_en';
$route['index_en'] = 'index/index_en';
$route['news_en']='index/news_en';
$route['news_detail_en']='index/news_detail_en';
$route['default_controller'] = 'index';
$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

