<?php

defined('BASEPATH') OR exit('No direct script access allowed');

 	 		 $config["full_tag_open"] = '<ul id="ajax_pg" class="pagination">';

			 $config["full_tag_close"] = '</ul>';  

			 $config["first_link"] = "&laquo;";

			 $config["first_tag_open"] = "<li class='page-item'>";

			 $config["first_tag_close"] = "</li>";

			 $config["last_link"] = "&raquo;";

			 $config["last_tag_open"] = "<li class='page-item'>";

			 $config["last_tag_close"] = "</li>";

			 $config['next_link'] = '&gt;';

			 $config['next_tag_open'] = "<li class='page-item'>";

			 $config['next_tag_close'] = '<li>';

			 $config['prev_link'] = '&lt;';

			 $config['prev_tag_open'] = "<li class='page-item'>";

			 $config['prev_tag_close'] = '<li>';

			 $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" >';

			 $config['cur_tag_close'] = '</a></li>';

			 $config['num_tag_open'] = "<li class='page-item'>";

			  $config['num_tag_close'] = '</li>';
			  $config['anchor_class'] = 'class="page-link" ';