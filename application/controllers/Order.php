<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	    public function __construct()
       {
            parent::__construct();
			$this->load->model("Order_model");
			$this->load->model("Common_model");
			$this->load->helper('directory');
			$this->load->library('pagination');
			if($this->uri->segment(2)!="login" && $this->session->userdata("user_acc")==null)
			{
				if($this->uri->segment(2)!="logout" && $this->uri->segment(2)!="login_valid")
					redirect(base_url()."admin/login","refresh");
				//else   echo "非驗證導向";
			}
			
	   }
	   public function reload()
	   {
			$this->session->unset_userdata('search_val');
			$this->session->unset_userdata('search_name');
			$this->output->set_content_type("application/json");
		    $this->output->set_output("'success' : 'true'");
	   }
	  
	   public function search()
	   {
		   $data=$this->input->post("data");
		   $this->session->set_userdata("search_val",$data["search_val"]);
		   $this->session->set_userdata("search_name",$data["search_name"]);
		
		   $this->output->set_content_type("application/json");
		   $this->output->set_output("'success' : 'true'");
	   }
	   public function statistics()
	   {
		    $data["data_structure"]=$this->load->view("order/order_statistics_structure.js",'',true);
		   	$this->load->view("admin/template/header.php");
			$this->load->view("order/order_statistics.php",$data);
			$this->load->view("order/template/footer.php");

	   }
	   public function Listing()
	   {
		   
			$data['result']=$this->Order_model->statistics_list($this->session->userdata("user_id"),$_POST["dat"]);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
			

	   }
	   function preg_test($value,$regx)
	   {
		  
		   $result=preg_match($regx,$value);
		   if($result==1)
		   return true;
		   else
		   return false;
	   }
	   public function check_permittion()
	   {
		   $data=$this->input->post("dat");
		   $data["success"]=true;
		//    if($data["order_status"]=="已交貨 - 未收款項")
		//    {
		// 	   $data["success"]=false;
		// 	   $data["msg"]="貨物數量不足，不能出貨。";
		//    }
		   if(preg_match('/^\+?[1-9][0-9]*$/',$data["order_num"])==0)
		   {
			$data["success"]=false;
			$data["msg"]="訂單數量格式錯誤";
		   }
		
		   if($data["order_status"]!="未確認訂單"  && ($data["order_num_lock"]=="0" || $data["order_num_lock"]==null))
		   {
			   $data["success"]="warning";
			   $data["msg"]="此狀態會鎖定訂單數量，確定繼續?";
			   $data["lock"]="order_num_lock";
		   }
		   if($data["order_status"]=="訂單完成")
		   {
			$data["success"]="warning";
			$data["msg"]="此狀態會鎖定訂單。";
			$data["lock"]="order_lock";
			}
		//    if($data["order_num"]>0)
		//    {
		// 		$data["success"]="warning";
		// 		$data["msg"]="數量貨物不足，確認出貨?";
		//    }
		  
		  
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
       public function index()
	   {
		   	$start=0;
			if($this->input->get("per_page")!=NULL)
			{
				$start=$this->input->get("per_page");
			}
			$qry=$this->Order_model->get_product();
			$data["products"]=$qry->result_array();
			
		    $data["add_view"] = $this->load->view("order/order_add.php",$data, true);
			$data["data_structure"] = $this->load->view("order/order_structure.js",$data, true);			
			
			$where["create_id"]=$this->session->userdata("user_id");
			// $like=null;
			// if($this->session->userdata("search_val")!=null)
			// $like[$this->session->userdata('search_name')]=$this->session->userdata("search_val");
			
			// $query=$this->Order_model->list_order($creater=$this->session->userdata("user_id"),$order="order.order_date",$limit='15', $start,$like);
			
		    // $data['list']=$query->result_array();
		    $this->load->view("admin/template/header.php",$data);
		    $this->load->view("order/order_list.php",$data);
		    $this->load->view("order/template/footer.php",$data);
	   }
	   public function get_order()
	   {
		   $where=$this->input->post();
		   $qry=$this->Common_model->condition_query($db='order',$where,$order="");
		   $data=$qry->row_array();
		   $this->output->set_content_type('application/json');
		   $this->output->set_output(json_encode($data));
	   }
    //    public function edit()
	//    {
	// 	   $where["order_id"]=$this->input->get("id");
	// 	   $qry=$this->Common_model->condition_query($db='order',$where,"");
	// 	   $data=$qry->row_array();
	// 	   $qry=$this->Order_model->get_product();
	// 		$data["products"]=$qry->result_array();
	// 	   $data["resource"]=base_url()."resource/Order/";
	// 		$data["manu_type"]="訂單新增";
	// 		$this->load->view("admin/template/header.php",$data);
	// 		$this->load->view("order/order_edit.php",$data);
	// 		$this->load->view("order/template/footer.php",$data);
		   
	//    }
	   public function read_order()
	   {
			$qry=$this->Order_model->get_product();
			$data["products"]=$qry->result_array();
			
			$data["add_view"] = $this->load->view("order/order_add.php",$data, true);
			if($this->input->get("per_page")!=NULL)
			{
				$start=$this->input->get("per_page");
			}
			else
				$start=0;
			
			$where["create_id"]=$this->session->userdata("user_id");
			if($this->session->userdata("search_val")!=null)
			{
				$like[$this->session->userdata('search_name')]=$this->session->userdata("search_val");
				$this->db->where("order.create_id=".$this->session->userdata("user_id"),NULL,FALSE);
				$this->db->where("order.order_product_id=product.product_id",NULL,FALSE);
				$this->db->like($like); 
				$this->db->from('order');
				$this->db->from('product');
				$rows=$this->db->count_all_results();
			}
			else
			{
				$like=null;
				$this->db->select();
				$this->db->where('create_id', $this->session->userdata("user_id"));
				$this->db->from('order');
				$rows=$this->db->count_all_results();
			}
			$query=$this->Order_model->list_order($creater=$this->session->userdata("user_id"),$order="order.order_date",$limit='15', $start,$like);
			$config['base_url'] = base_url()."Order/read_order";
			$config['total_rows'] = $rows;
			$config['per_page'] =15; 
			$config['page_query_string'] = TRUE;
			$config['attributes'] = array('class' => 'page-link');
			$this->pagination->initialize($config); 
			$data["page"]=$this->pagination->create_links();
			$data['result']=$query->result_array();
			// $data["head"]["0"]="商 品 名 稱";
			// $data["head"]["1"]="商 品 數 量";
			// $data["head"]["2"]="客 戶 名 稱";
			// $data["head"]["3"]="客 戶 電 話";
			// $data["head"]["4"]="交 貨 日 期";
			// $data["head"]["5"]="訂 單 狀 態";
			// $data["head"]["6"]="訂 單 收 益";
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));

	   }
	   public function order_update()
	   {
		   print_r($this->input->post("dat"));
		   $data=$this->input->post("dat");
		   $id=$data["order_id"];
		   unset($data["order_id"]);
		   if($data["order_status"]!="未確認訂單")
		   {
				$data["stock_bool"]=1;
				$this->db->query('UPDATE `product` SET `product_stock` = product_stock - '.$data["order_num"]." WHERE `product_id` = " . $data["order_product_id"]);

		   }
		   if($id==NULL)
		   {
				
				$data["create_id"]=$this->session->userdata('user_id');
				$data["create_date"]=date("Y/m/d");
				
				$this->Common_model->insert_return_id($db='order',$data);
				
				
		   }
		   else
		   {
				$insert=$this->input->post("dat");
				$where["order_id"]=$id;
				$this->Common_model->update($db='order',$insert,$where);
				
		   }
		   $this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
	   }
       public function delete_order()
	   {
		    
			$where["order_id"]=$this->input->post("order_id");
			$query=$this->Common_model->condition_query($db='order',$where,$order="");
			$data=$query->row_array();
			if($data["order_status"]!="未確認訂單")
			{
				$qry=$this->Common_model->condition_query($db='order',$where,$order="");
				$data=$qry->row_array();
				$this->db->query('UPDATE `product` SET `product_stock` = product_stock + '.$data["order_num"]." WHERE `product_id` = " . $data["order_product_id"]);
			}
			$this->Common_model->delete("order",$where);
			$data["success"]=true;
			$data["id"]=$where["order_id"];
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
	   }
}