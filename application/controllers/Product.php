<?
class Product extends CI_Controller {
     public function __construct()
       {
            parent::__construct();
			$this->load->model("Product_model");
			$this->load->model("Common_model");
			$this->load->helper('directory');
			$this->load->library('pagination');
			if($this->uri->segment(2)!="login" && $this->session->userdata("user_acc")==null)
			{
				if($this->uri->segment(2)!="logout" && $this->uri->segment(2)!="login_valid")
					redirect(base_url()."admin/login","refresh");
			}
	   }
	   public function get_remark()
	   {
		   $where=$_POST;
		   $result=$this->Common_model->condition_query($db='product',$where,$order="")->row_array();
				
		   $data["head"]=$result["product_name"];
			$data["remark"]=$result["product_remark"];
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
	   }
	   public function reload()
	   {
			$this->session->unset_userdata('search_val');
			$this->session->unset_userdata('search_name');
			$data["success"]=true;
			$this->output->set_content_type('application/json');
		   $this->output->set_output(json_encode($data));
	   }

	   public function search()
	   {
		   $data=$this->input->post("dat");
		   $this->session->set_userdata("search_val",$data["search_val"]);
		   $this->session->set_userdata("search_name",$data["search_name"]);
		   $this->output->set_content_type('application/json');
		   $this->output->set_output(json_encode($data));
	   }
       public function index()
	   {
			$data["add_form"]=$this->load->view("product/product_add.php",null,true);
			$data["data_structure"]=$this->load->view("product/data_structure.js",null,true);
		    $this->load->view("admin/template/header.php",$data);
		    $this->load->view("product/product_list.php",$data);
		    $this->load->view("admin/template/footer.php",$data);
	   }
	   public function listing()
	   {
		if($this->input->get("per_page")!=NULL)
		{
			$start=$this->input->get("per_page");
		}
		else
			$start=0;

			$like=array();
		if($this->session->userdata("search_val")!=null)
		{
			$like[$this->session->userdata("search_name")]=$this->session->userdata("search_val");
			$this->db->select();
			$this->db->where("create_id", $this->session->userdata("user_id"));
			$this->db->like($like);
			$this->db->from("product");
			$rows=$this->db->count_all_results();
		}
		else
		{
			$this->db->where('create_id', $this->session->userdata("user_id"),null,false);
			$this->db->from('product');
			$rows=$this->db->count_all_results();
		}
		$where["create_id"]=$this->session->userdata("user_id");
		$query=$this->Common_model->listing($db='product',$where,$order="",$limit='15', $this->input->get("per_page"),$like);
		

		//config_val pagination
		$config['base_url'] = base_url()."Product/listing";
		$config['total_rows'] = $rows;
		$config['per_page'] =15; 
		$config['page_query_string'] = TRUE;
		$config['attributes'] = array('class' => 'page-link');
		$this->pagination->initialize($config); 
		$data["page"]=$this->pagination->create_links();

		
		$query=$this->Common_model->listing($db='product',$where,$order="",$limit='15', $this->input->get("per_page"),$like);
		$data['result']=$query->result_array();
		$this->output->set_content_type("application/json");
		$this->output->set_output(json_encode($data));
	   }
    //    public function edit()
	//    {
	// 	   $where["product_id"]=$this->input->get("id");
	// 	   $qry=$this->Common_model->condition_query($db='product',$where,"");
	// 	   $data=$qry->row_array();
		   
	// 		$data["manu_type"]="訂單新增";
	// 		$query=$this->Common_model->no_con_query("post_type");
	// 		$this->load->view("admin/template/header.php",$data);
	// 		$this->load->view("product/product_edit.php",$data);
	// 		$this->load->view("admin/template/footer.php",$data);
		   
	//    }
	   
	  
	   
	   public function product_add()
	   {
			$data["manu_type"]="訂單新增";
			$this->load->view("admin/template/header.php",$data);
			$this->load->view("product/product_add.php",$data);
			$this->load->view("admin/template/footer.php",$data);
	   }
	   public function get_product()
	   {
		   $where=$this->input->get();
		   $qry=$this->Common_model->condition_query($db='product',$where,"");
		   $data=$qry->row_array();
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
	   public function product_update()
	   {
		   $data=$this->input->post("dat");
		   $id=$data["product_id"];
		   unset($data["product_id"]);
		   if($id==NULL)
		   {
				
				$data["create_id"]=$this->session->userdata('user_id');
				$data["create_date"]=date("Y/m/d");
				
				$this->Common_model->insert_return_id($db='product',$data);
				
				
		   }
		   else
		   {
				
				$where["product_id"]=$id;
				$this->Common_model->update($db='product',$data,$where);
				
		   }
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
       public function delete_product()
	   {
		   
			$where["product_id"]=$this->input->get("id");
			$this->Common_model->delete("product",$where);
			redirect(base_url()."product","refresh");
	   }
}
?>