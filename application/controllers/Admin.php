<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	    public function __construct()
       {
            parent::__construct();
			$this->load->model("Order_model");
			$this->load->model("Common_model");
			$this->load->helper('directory');
			$this->load->library('pagination');
			if($this->uri->segment(2)!="login" && $this->session->userdata("user_acc")==null)
			{
				if($this->uri->segment(2)!="logout" && $this->uri->segment(2)!="login_valid")
					redirect(base_url()."admin/login","refresh");
				//else   echo "非驗證導向";
			}
			
	   }
	   
	   public function logout()
	   {
		   $this->session->sess_destroy();
		   redirect(base_url()."admin/login","refresh");
	   }
	   public function user_update()
	   {
		   $data=$this->input->post("dat");
		   $id=$data["user_id"];
		   unset($data["user_id"]);
		   if($id=="")
		   {
			
			$data["user_password"]=hash('sha256',$data["user_password"]);
			$this->Common_model->insert_return_id('user',$data);
		   }
		   else
		   {  
			   $where["user_id"]=$id;
			   $this->Common_model->update('user',$data,$where);
		   }
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
		}
	   public function user_delete()
	   {
		   $where['user_id']=$this->input->get("id");
		   $this->Common_model->delete($db='user',$where);
		   redirect(base_url()."admin/user_list","refresh");
	   }
	   public function user_add()
	   {
			$data["resource"]=base_url()."resource/admin/";
			$data["manu_type"]="使用者";
		   $this->load->view("admin/template/header.php",$data);
		   $this->load->view("admin/user_add.php");
		   $this->load->view("admin/template/footer.php",$data);
	   }
	   public function reload_user()
	   {
		   $this->session->unset_userdata('user_id');
		   $this->session->unset_userdata('user_email');
		   $this->session->unset_userdata('user_name');
		   $this->session->unset_userdata('search_bool');
		   redirect(base_url()."admin/user_list");
	   }
	//    public function user_change()
	//    {
	// 	   $where["user_id"]=$this->input->get("id");
	// 	   $query=$this->Common_model->condition_query('user',$where);
	// 	   $row=$query->row_array();
	// 	   $orign_id=$this->session->userdata("user_id");
	// 	   if($this->session->userdata("user_change")==NULL)
	// 	   {
	// 			$this->session->set_userdata("user_change","1");
	// 			$this->session->set_userdata("orign_id",$orign_id);
	// 	   }
	// 	   $this->session->set_userdata("user_acc",$row["user_accnum"]);
	// 	   $this->session->set_userdata("user_id",$row["user_id"]);
	// 	   $this->session->set_userdata("user_auth",$row["user_auth"]);
	// 	   redirect(base_url()."admin/user_list","refresh");
	//    }
	//    public function user_return()
	//    {
	// 	   $where["user_id"]=$this->session->userdata("orign_id");
	// 	   $query=$this->Common_model->condition_query('user',$where);
	// 	   $row=$query->row_array();
	// 	   $this->session->set_userdata("user_acc",$row["user_account"]);
	// 	   $this->session->set_userdata("user_id",$row["user_id"]);
	// 	   $this->session->set_userdata("user_auth",$row["user_auth"]);
	// 	   $this->session->unset_userdata('orign_id');
	// 	   $this->session->unset_userdata('user_change');
	// 	   redirect(base_url()."admin/user_list","refresh");
	//    }
	   public function user_listing()
	   {
			if($this->input->get("per_page")!=NULL)
			{
				$start=$this->input->get("per_page");
			}
			else
			$start=0;
			
			$query=$this->Common_model->listing($db='user',$where=array(),$order="",$limit='15', $start,$like=array());
			$data["result"]=$query->result_array();
			
			$this->db->select();
			$this->db->from('user');
			$rows=$this->db->count_all_results();
			$config['base_url'] = base_url()."Admin/user_list";
			$config['total_rows'] = $rows;
			$config['per_page'] =15; 
			$config['page_query_string'] = TRUE;
			$config['attributes'] = array('class' => 'page-link');
			$this->pagination->initialize($config); 
			$data["page"]=$this->pagination->create_links();

			$this->output->set_content_type("application/json");
			$this->output->set_output(json_encode($data));
	   }
	   public function user_edit()
	   {
			$where=$this->input->get();
			$qry=$this->Common_model->condition_query($db='user',$where,"");
			$data=$qry->row_array();
			$this->output->set_content_type("application/json");
			$this->output->set_output(json_encode($data));
	   }
	   public function user_list()
	   {	
			$data["data_structure"]=$this->load->view("admin/data_structure.js",null,true);
			$data["add_form"]=$this->load->view("admin/user_add",null,true);
			$this->load->view("admin/template/header.php");
			$this->load->view("admin/user_list",$data);
			$this->load->view("admin/template/footer.php");
	   }
	  
	   public function index()
	   {
			
	   }
	  
	   public function login()
	   {
		   if($this->session->userdata("user_acc")!=null)
		   {
			 redirect(base_url()."order","refresh");
		   }
		   else
		   {
		   $data["resource"]=base_url()."resource/admin/";
		   $this->load->view("admin/login.php",$data);
		   }
	   }
	   public function login_valid()
	   {
		   $user=array();
		   $user["user_account"]=$this->input->post("user_accnum");
		   $user["user_password"] = hash("sha256", $this->input->post("user_password"));
		   $query=$this->Common_model->condition_query("user",$user);
		   

		   if($query->num_rows()>0)
		   {
			   $row=$query->row_array();
				$this->session->set_userdata("user_acc",$row["user_account"]);
				$this->session->set_userdata("user_id",$row["user_id"]);
				$this->session->set_userdata("user_auth",$row["user_auth"]);
			   redirect(base_url()."order","refresh");
		   }
		  
		   redirect(base_url()."admin/login","refresh");
	   }
	   
	   
	  
}