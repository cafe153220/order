<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	    public function __construct()
       {
            parent::__construct();
			$this->load->model("Common_model");
			$this->load->model("Supplier_model");
			$this->load->helper('directory');
			$this->load->library('pagination');
			if($this->uri->segment(2)!="login" && $this->session->userdata("user_acc")==null)
			{
				if($this->uri->segment(2)!="logout" && $this->uri->segment(2)!="login_valid")
					redirect(base_url()."admin/login","refresh");
				//else   echo "非驗證導向";
			}
			
	   }
	   
	   public function logout()
	   {
		   $this->session->sess_destroy();
		   redirect(base_url()."admin/login","refresh");
	   }
	   public function edit()
	   {
		   $where["supplier_id"]=$this->input->get("supplier_id");
		   $qry=$this->Common_model->condition_query($db='supplier',$where,"");
		   $data=$qry->row_array();
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
	   public function supplier_add()
	   {
			$this->load->view("admin/template/header.php");
			$this->load->view("supplier/supplier_add.php");
			$this->load->view("admin/template/footer.php");
	   }
	   public function supplier_update()
	   {
		   $data=$this->input->post("dat");
		   
		   $id=$data["supplier_id"];
		   unset($data["supplier_id"]);
		   if($id==NULL)
		   {
				
				$data["create_id"]=$this->session->userdata('user_id');
				$this->Common_model->insert_return_id($db='supplier',$data);
				
				
		   }
		   else
		   {
				$insert=$data;
				$where["supplier_id"]=$id;
				$this->Common_model->update($db='supplier',$insert,$where);
				
		   }
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
	   public function income_check_permittion()
	   {
			$data=$this->input->post("dat");
			$msg["success"]="success";
			if($data["income_status"]=="進貨 - 付款完成")
			{
				$msg["success"]="warning";
			  	$msg["msg"]="此狀態會鎖定這項進貨，確定繼續?";
			   	$msg["lock"]="income_lock";
				
			}
			if($data["income_status"]=="進貨 - 未付款")
			{
				$msg["success"]="warning";
			  	$msg["msg"]="此狀態會鎖定進貨數量，確定繼續?";
			   	$msg["lock"]="income_num_lock";
			}
			$this->output->set_content_type("application/json");
		   	$this->output->set_output(json_encode($msg));
		}
	   public function supplier_income_update()
	   {
		   $data=$this->input->post("dat");
		   $id=$data["income_id"];
		   unset($data["income_id"]);
		   
		   if($id==NULL)
		   {
				$data["create_id"]=$this->session->userdata('user_id');
				$this->Common_model->insert_return_id($db='product_income',$data);
		   }
		   else
		   {
				$insert=$data;
				$where["income_id"]=$id;
				$this->Common_model->update($db='product_income',$insert,$where);
				
		   }
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
	   public function listing()
	   {
			if($this->input->get("per_page")!=NULL)
			{
				$start=$this->input->get("per_page");
			}
			else
				$start=0;
			
			$where["create_id"]=$this->session->userdata("user_id");
			$query=$this->Common_model->listing($db='supplier',$where,$order="",$limit='15', $this->input->get("per_page"));
			$this->db->where('create_id', $this->session->userdata("user_id"));
			$this->db->from('supplier');
			$rows=$this->db->count_all_results();
			$config['base_url'] = base_url()."supplier";
			$config['total_rows'] = $rows;
			$config['per_page'] =15; 
			$config['page_query_string'] = TRUE;
			$config['attributes'] = array('class' => 'page-link');
			$this->pagination->initialize($config); 
			$data["page"]=$this->pagination->create_links();
			$data['result']=$query->result_array();
			$this->output->set_content_type("application/json");
			$this->output->set_output(json_encode($data));
	   }
	   
	   public function income_edit()
	   {
		   $where["income_id"]=$this->input->get("income_id");
		   $qry=$this->Common_model->condition_query($db='product_income',$where,"");
		   $data=$qry->row_array();
		   $this->output->set_content_type("application/json");
		   $this->output->set_output(json_encode($data));
	   }
	   public function index()
	   {
			$data["data_structure"]=$this->load->view("supplier/data_structure.js",NULL,true);
			$data["add_form"]=$this->load->view("supplier/supplier_add",NULL,true);
		    $this->load->view("admin/template/header.php",$data);
		    $this->load->view("supplier/supplier_list.php",$data);
		    $this->load->view("admin/template/footer.php",$data);
	   }
	   public function supplier_income()
	   {
			$where['create_id']= $this->session->userdata("user_id");
			$data["products"]=$this->Common_model->condition_query($db='product',$where,"")->result_array();
			$data["supplier"]=$this->Common_model->condition_query($db='supplier',$where,"")->result_array();
			$data["data_structure"]=$this->load->view("supplier/data_income_structure.js",NULL,true);
			$data["add_form"]=$this->load->view("supplier/supplier_income_edit",$data,true);
			$this->load->view("admin/template/header.php",$data);
			$this->load->view("supplier/supplier_income_list.php",$data);
			$this->load->view("admin/template/footer.php",$data);
	   }
	   public function income_listing()
	   {
			if($this->input->get("per_page")!=NULL)
			{
				$start=$this->input->get("per_page");
			}
			else
				$start=0;
			
			$where["create_id"]=$this->session->userdata("user_id");
			$query=$this->Supplier_model->list_income($this->session->userdata("user_id"),$order="",$limit='15', $start,$like=array());
			$this->db->where('create_id', $this->session->userdata("user_id"));
			$this->db->from('product_income');
			$rows=$this->db->count_all_results();
			$config['base_url'] = base_url()."supplier_income";
			$config['total_rows'] = $rows;
			$config['per_page'] =15; 
			$config['page_query_string'] = TRUE;
			$config['attributes'] = array('class' => 'page-link');
			$this->pagination->initialize($config); 
			$data["page"]=$this->pagination->create_links();
			$data['result']=$query->result_array();
			$this->output->set_content_type("application/json");
			$this->output->set_output(json_encode($data));
	   }
	   
	  
	   public function delete_supplier()
	   {
		   
			$where["supplier_id"]=$this->input->get("id");
			$this->Common_model->delete("supplier",$where);
			$data["success"]="true";
			$this->output->set_content_type("application/json");
			
			$this->output->set_output(json_encode($data));
	   }
	   public function delete_income()
	   {
		   
			$where["income_id"]=$this->input->get("id");
			$this->Common_model->delete("product_income",$where);
			$data["success"]="true";
			$this->output->set_content_type("application/json");
			
			$this->output->set_output(json_encode($data));
	   }
	   
	   
}